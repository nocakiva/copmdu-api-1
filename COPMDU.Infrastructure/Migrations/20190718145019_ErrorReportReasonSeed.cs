﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace COPMDU.Infrastructure.Migrations
{
    public partial class ErrorReportReasonSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "ErrorReportReason",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Ticket não encontrado no APP" },
                    { 2, "Validação de sinais errada" },
                    { 3, "Erro na validação de sinais" },
                    { 4, "Aplicativo fecha sozinho" },
                    { 5, "Erro no fechamento do ticket" },
                    { 6, "Outros" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ErrorReportReason",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ErrorReportReason",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ErrorReportReason",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ErrorReportReason",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ErrorReportReason",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ErrorReportReason",
                keyColumn: "Id",
                keyValue: 6);
        }
    }
}
