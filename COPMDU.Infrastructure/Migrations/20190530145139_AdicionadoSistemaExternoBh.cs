﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace COPMDU.Infrastructure.Migrations
{
    public partial class AdicionadoSistemaExternoBh : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "ExternalSystem",
                columns: new[] { "Id", "Name" },
                values: new object[] { 7, "netbh" });

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 25666,
                column: "ExternalSystemId",
                value: 7);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ExternalSystem",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 25666,
                column: "ExternalSystemId",
                value: 4);
        }
    }
}
