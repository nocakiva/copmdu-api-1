﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Infrastructure.InterationException
{

    [Serializable]
    public class TicketResponseException : Exception
    {
        public TicketResponseException() { }
        public TicketResponseException(string message) : base(message) { }
        public TicketResponseException(string message, Exception inner) : base(message, inner) { }
        protected TicketResponseException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
