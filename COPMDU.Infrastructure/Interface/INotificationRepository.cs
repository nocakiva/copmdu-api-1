﻿using COPMDU.Domain.Notification;
using COPMDU.Infrastructure.PageModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.Interface
{
    public interface INotificationRepository : IDisposable
    {
        /// <summary>
        /// Autentica no sistema de notificação
        /// </summary>
        /// <param name="username">Usuário para ser autenticado</param>
        /// <param name="password">Senha do usuário</param>
        /// <returns>ID da sessão</returns>
        Task<bool> Authenticate(string username, string password, int cityId);

        /// <summary>
        /// Finaliza a notifição
        /// </summary>
        /// <param name="id">ID da notificação</param>
        /// <param name="resolutionId">ID da resolução de fechamento da notificação</param>
        /// <param name="obs">Observação de fechamento da notificação</param>
        /// <returns></returns>
        Task<CloseStatus> Close(int id, int resolutionId, string obs, int cityId);

        Task<List<Cell>> GetCells(string node, int cityId);

        Task<List<CitOccurrence>> GetOccurrences(string node, string cell, int cityId);
    }
}
